#!/bin/bash
make
make clean
first=${1:-"40"}
for i in `find -name "test*.in"`
do
    ./manager $first ${i/\.\/DATA\//} "temp"
    sort -n ./DATA/temp > ./DATA/sorted
    if diff ./DATA/sorted ${i/in/out} &>/dev/null
    then
        echo "$i: passed"
    else
        echo "$i: failed"
    fi
done
rm manager executor ./DATA/temp ./DATA/sorted
