/* Dawid Lazarczyk, 337614, SO zad. nr 1 */
#ifndef _UTILS_
#define _UTILS_

#include "err.h"

/* size of the buffer for the line; (64 + 1) kB will be enough */
#define BUF_SIZE (65 * 1024)

/* negative number indicating that the process should terminate */
#define END_CODE -1

/* number that should be sent to the manager when the executors have
 * been created */
#define START_CODE 42

/* The macros below let us find out, where in code the error has occurred,
 * the line stands for the line where the macro was called. The SYSERR
 * macro should be used after failure of system functions (i.e. functions that
 * set the errno variable) and FATAL in other cases. Name should be the name of
 * the called function */
#define SYSERR(name) (syserr("File %s, line %d, '%s' failed", \
            __FILE__,__LINE__, name))
#define FATAL(name) (fatal("File %s, line %d, '%s' failed", \
            __FILE__, __LINE__, name))
/* The macros below are used to make calls of the functions that can
 * fail more concise. */
#define SYS_FAIL_IF(cond, name) if ((cond)) SYSERR((name))
#define FAIL_IF(cond, name) if ((cond)) FATAL((name))

extern char* find_operator(const char* line);
#endif
