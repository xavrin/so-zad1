/* Dawid Lazarczyk, 337614, SO zad. nr 1 */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <wait.h>
#include <string.h>
#include "utils.h"
#include "err.h"

#define ARGS_NUM 3
#define LAST_DSC_INDEX 1 /* position of descriptor to the last pipe */
#define N_INDEX 2
#define SPACE ' '

void create_ring(int N, int last_dsc, int *proc_nr) {
    int i;
    int middle_pipe[2];

    for (i = 1; i <= N - 1; i++) {
        SYS_FAIL_IF(pipe(middle_pipe) == -1, "pipe");
        switch (fork()) {
            case -1:
                SYSERR("fork");
            case 0:
                /* redirecting input to the middle pipe */
                SYS_FAIL_IF(dup2(middle_pipe[0], 0) == -1, "dup2");
                SYS_FAIL_IF(close(middle_pipe[0]) == -1, "close");
                SYS_FAIL_IF(close(middle_pipe[1]) == -1, "close");
                continue; /* the child process should stay in the loop */
            default:
                /* redirecting output to the middle pipe */
                SYS_FAIL_IF(dup2(middle_pipe[1], 1) == -1, "dup2");
                SYS_FAIL_IF(close(middle_pipe[1]) == -1, "close");
                SYS_FAIL_IF(close(middle_pipe[0]) == -1, "close");
                /* closing descriptor to the last pipe */
                SYS_FAIL_IF(close(last_dsc) == -1, "close");
        }
        break; /* the parent process should exit the loop */
    }
    if (i == N) { /* it's the last process in the ring */
        /* redirecting output to the manager */
        SYS_FAIL_IF(dup2(last_dsc, 1) == -1, "dup2");
        SYS_FAIL_IF(close(last_dsc) == -1, "close");
        /* now we can let the manager know that he can start reading data */
        FAIL_IF(printf("%d\n", START_CODE) < 0, "printf");
        SYS_FAIL_IF(fflush(stdout) == EOF, "fflush");
    }
    *proc_nr = i;
}

int calculate(int left, char operator, int right) {
    switch (operator) {
        case '+':
            return left + right;
        case '-':
            return left - right;
        case '*':
            return left * right;
        case '/':
            return left / right;
    }
}

/* Returns a pointer to the last occurrence of character c in the string
 * beginning at beg, inclusively, and ending at end, exclusively.
 * Assuming that beg and end point to the same block of memory. */
char* find_previous_occurrence(const char *beg, char *end,  char c) {
    char *ptr = end - 1;
    while (ptr >= beg) {
        if (*ptr == c)
            return ptr;
        ptr--;
    }
    return NULL;
}

/* Make the calculations for a line or do nothing, if it's already
 * calculated. Returns 0 on success, -1 on failure */
int process_line(char *line) {
    char *ptr, *oper;
    static char copy[BUF_SIZE];
    int left, right, result;
    oper = find_operator(line);
    if (!oper)
        return 0;
    strcpy(copy, oper + 1); /* copying the string after the operator */
    /* looking for the first integer before operator, assuming that the
     intervals between strings contain only one space */
    ptr = find_previous_occurrence(line, oper - 1, SPACE);
    if (!ptr)
        return -1;
    right = atoi(ptr);
    ptr = find_previous_occurrence(line, ptr, SPACE);
    if (!ptr)
        return -1;
    left = atoi(ptr);
    result = calculate(left, *oper, right);
    ptr++; /* leaving a space-long interval */
    /* copy already contains a whitespace */
    sprintf(ptr, "%d%s", result, copy);
    return 0;
}

int main(int argc, char *argv[]) {
    char buffer[BUF_SIZE];
    int N, last_dsc, proc_nr, line_no;

    if (argc < ARGS_NUM)
        fatal("Too few arguments");
    N = atoi(argv[2]);
    if (N < 1)
        fatal("The number of executors must be not less than 1");
    last_dsc = atoi(argv[1]); /* descriptor for writing to manager */

    create_ring(N, last_dsc, &proc_nr);

    FAIL_IF(fgets(buffer, BUF_SIZE, stdin) == NULL, "fgets");
    FAIL_IF(sscanf(buffer, "%d", &line_no) == EOF, "sscanf");

    while (line_no != END_CODE) { /* indicating the end of work */
        /* although there is line_no included in buffer, it doesn't matter
         * as long as the expression is correct - we'll be doing something
         * only if there's an operator in the buffer */
        if (process_line(buffer) == -1)
            fatal("Invalid string");
        FAIL_IF(printf("%s", buffer) < 0, "printf");
        SYS_FAIL_IF(fflush(stdout) == EOF, "fflush");
        FAIL_IF(fgets(buffer, BUF_SIZE, stdin) == NULL, "fgets");
        FAIL_IF(sscanf(buffer, "%d", &line_no) == EOF, "sscanf");
    }
    if (proc_nr != N) {
        FAIL_IF(printf("%d\n", END_CODE) < 0, "printf");
        SYS_FAIL_IF(fflush(stdout) == EOF, "fflush");
        SYS_FAIL_IF(wait(0) == -1, "wait");
    }
    SYS_FAIL_IF(close(0) == -1, "close");
    SYS_FAIL_IF(close(1) == -1, "close");
    exit(0);
}
