/* Dawid Lazarczyk, 337614, SO zad. nr 1 */
#include <string.h>
#include "utils.h"

char operators[] = "+-/*";

int is_whitespace(char c) {
    return c == ' ' || c == '\n';
}

char* find_operator(const char* line) {
    char *ptr;
    ptr = strpbrk(line, operators);
    while (ptr && !is_whitespace(*(ptr+1)))
        ptr = strpbrk(ptr + 1, operators);
    return ptr;
}
