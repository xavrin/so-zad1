CC = cc

FLAGS = -ansi -pedantic

ALL = executor manager

all: $(ALL)

%.o: %.c
	$(CC) $(FLAGS) -c $<

$(ALL): %: %.o utils.o err.o
	$(CC) $(FLAGS) -o $@ $^
.PHONY: clean

clean:
	rm -fr *.o
