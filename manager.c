/* Dawid Lazarczyk, 337614, SO zad. nr 1 */
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "err.h"

#define ARGS_NUM 4 /* number of needed arguments */
#define N_INDEX 1 /* position of argument with the number of processes */
#define IN_INDEX 2 /* position of argument with name of input file */
#define OUT_INDEX 3 /* position of argument with name of output file */
#define DATA_DIR "./DATA"
#define NEWLINE '\n'

void start_executors(char *buffer, const char* N) {
    int first_pipe[2], last_pipe[2];

    SYS_FAIL_IF(pipe(first_pipe) == -1, "pipe");
    SYS_FAIL_IF(pipe(last_pipe) == -1, "pipe");

    switch (fork()) {
        case -1:
            SYSERR("fork");
        case 0:
            /* redirecting input to the first pipe */
            SYS_FAIL_IF(dup2(first_pipe[0], 0) == -1, "dup2");
            SYS_FAIL_IF(close(first_pipe[0]) == -1, "close");

            SYS_FAIL_IF(close(first_pipe[1]) == -1, "close");
            SYS_FAIL_IF(close(last_pipe[0]) == -1, "close");
            sprintf(buffer, "%d", last_pipe[1]);
            execl("./executor", "executor", buffer, N, (char*)0);
            SYSERR("execl");
        default:
            /* redirecting output to the first pipe */
            SYS_FAIL_IF(dup2(first_pipe[1], 1) == -1, "dup2");
            SYS_FAIL_IF(close(first_pipe[1]) == -1, "close");
            /* redirecting input to the last pipe in the ring */
            SYS_FAIL_IF(dup2(last_pipe[0], 0) == -1, "dup2");
            SYS_FAIL_IF(close(last_pipe[0]) == -1, "close");

            SYS_FAIL_IF(close(first_pipe[0]) == -1, "close");
            SYS_FAIL_IF(close(last_pipe[1]) == -1, "close");
    }
}

/* Reading a line from 'in' file and sending it to executors. */
void read_expression(char *buffer, FILE *in, int *line_count,
                     int *in_progress) {
    FAIL_IF(fgets(buffer, BUF_SIZE, in) == NULL, "fgets");
    if (!strchr(buffer, NEWLINE))
        fatal("Invalid line"); /* line too long or no newline at the end */
    (*line_count)++;
    /* newline is already in the buffer */
    FAIL_IF(printf("%d %s", *line_count, buffer) < 0, "printf");
    SYS_FAIL_IF(fflush(stdout) == EOF, "fflush");
    (*in_progress)++;
}

void calculate_data(char *buffer, int N, FILE *in, FILE *out) {
    int line_count = 0, num_of_lines, in_progress = 0;
    /* reading the line with number of lines */
    FAIL_IF(fgets(buffer, BUF_SIZE, in) == NULL, "fgets");
    FAIL_IF(sscanf(buffer, "%d", &num_of_lines) == EOF, "sscanf");

    while (line_count < num_of_lines && in_progress < N)
        read_expression(buffer, in, &line_count, &in_progress);

    while (in_progress > 0) {
        FAIL_IF(fgets(buffer, BUF_SIZE, stdin) == NULL, "fgets");
        if (find_operator(buffer)) { /* checking if the expression is ready */
            FAIL_IF(printf("%s", buffer) < 0, "printf");
            SYS_FAIL_IF(fflush(stdout) == EOF, "fflush");
        }
        else {
            int result, line_no;
            FAIL_IF(sscanf(buffer, "%d %d", &line_no, &result) == EOF,
                    "sscanf");
            FAIL_IF(fprintf(out, "%d: %d\n", line_no, result) < 0,
                    "fprintf");
            in_progress--;
            if (line_count < num_of_lines)
                read_expression(buffer, in, &line_count, &in_progress);
        }
    }
}

int main(int argc, char *argv[]) {
    FILE *out, *in;
    char buffer[BUF_SIZE];
    int N, msg; /* number of executors, message from the last executor */

    if (argc < ARGS_NUM)
        fatal("Too few arguments");
    N = atoi(argv[N_INDEX]);
    if (N < 1)
        fatal("The number of executors must be not less than 1");
    sprintf(buffer, "%s/%s", DATA_DIR, argv[IN_INDEX]);
    SYS_FAIL_IF((in = fopen(buffer, "r")) == NULL, "fopen");
    sprintf(buffer, "%s/%s", DATA_DIR, argv[OUT_INDEX]);
    SYS_FAIL_IF((out = fopen(buffer, "w")) == NULL, "fopen");

    start_executors(buffer, argv[N_INDEX]);
    /* waiting for the executors */
    FAIL_IF(fgets(buffer, BUF_SIZE, stdin) == NULL, "fgets");
    FAIL_IF(sscanf(buffer, "%d", &msg) == EOF, "sscanf");
    if (msg != START_CODE)
        fatal("Unknown starting code");
    /* we can start the calculations */
    calculate_data(buffer, N, in, out);
    /* we've finished; we have to make the executors terminate */
    FAIL_IF(printf("%d\n", END_CODE) < 0, "printf");
    SYS_FAIL_IF(fflush(stdout) == EOF, "fflush");
    SYS_FAIL_IF(wait(0) == -1, "wait");
    SYS_FAIL_IF(fclose(out) == EOF, "fclose");
    SYS_FAIL_IF(fclose(in) == EOF, "fclose");
    SYS_FAIL_IF(close(1) == -1, "close");
    SYS_FAIL_IF(close(0) == -1, "close");
    exit(0);
}
